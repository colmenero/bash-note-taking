/*
 * IMPORTANT!
 *
 * This is for my personal use
 *
 * Running shell commands from a C program is a potential security risk
 * An attacker could for example craft the name of notes in a way that makes
 * the program execute malicious commands
 *
 * I would also have to check the length of the buffers when reading
 */
#include <ncurses.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include "main.h"

bool init_gui(void);
bool redraw_windows(WINDOW *header_win, WINDOW *main_win, WINDOW *legend_win);
bool display_header(WINDOW *win);
bool display_legend(WINDOW *win);
bool select_list(WINDOW *header_win, WINDOW *main_win, WINDOW *legend_win);
bool highlight_entry(WINDOW *win, int entry);
bool display_list(WINDOW *win, unsigned int *max_entry);
bool display_task(WINDOW *win, int entry);
bool edit_task(WINDOW *win, int entry);
bool delete_task(WINDOW *win, int entry);
bool add_task(WINDOW *win, unsigned int *max_entry);
bool rename_task(WINDOW *win, int entry);
bool toggle_finish(WINDOW *win, int entry);
bool set_priority(WINDOW *win, unsigned int entry);
bool end_gui(void);

int main() {
    WINDOW *header_win = NULL;
    WINDOW *main_win = NULL;
    WINDOW *legend_win = NULL;

    if(!init_gui()) {
        goto done;
    }

    /* Header window */
    header_win = newwin(
            HEADER_WIN_LINES,
            HEADER_WIN_COLS,
            HEADER_WIN_Y0,
            HEADER_WIN_X0);
    if (header_win == NULL) {
        goto done;
    }
    wbkgd(header_win, COLOR_PAIR(COLOR_HEADER_WINDOW));

    /* Main window */
    main_win = newwin(
            MAIN_WIN_LINES,
            MAIN_WIN_COLS,
            MAIN_WIN_Y0,
            MAIN_WIN_X0);
    if (main_win == NULL) {
        goto done;
    }
    wbkgd(main_win, COLOR_PAIR(COLOR_MAIN_WINDOW));

    /* Legend window */
    legend_win = newwin(
            LEGEND_WIN_LINES,
            LEGEND_WIN_COLS,
            LEGEND_WIN_Y0,
            LEGEND_WIN_X0);
    if (legend_win == NULL) {
        goto done;
    }
    wbkgd(legend_win, COLOR_PAIR(COLOR_LEGEND_WINDOW));

    /* Start looping */
    if (!select_list(header_win, main_win, legend_win)) {
        goto done;
    }

done:
    if (main_win != NULL) {
        delwin(main_win);
    }
    end_gui();
    return 0;
}

bool init_gui() {
    bool success = false;

    initscr();  /* Create screen */
    cbreak();   /* Take input chars one at a time, do not buffer until \n */
    noecho();   /* Do not echo typed characters */

    /*
     * Initialize pairs of colors with:
     *     init_pair(identifier, foreground, background)
     *  They can then be used with: attron(COLOR_PAIR(identifier))
     */
    if (!has_colors()) {
        goto done;
    }
    start_color();
    init_pair(COLOR_NORMAL, COLOR_WHITE, COLOR_BLACK);
    init_pair(COLOR_SELECTION, COLOR_RED, COLOR_BLACK);
    init_pair(COLOR_HEADER_WINDOW, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(COLOR_MAIN_WINDOW, COLOR_CYAN, COLOR_BLACK);
    init_pair(COLOR_LEGEND_WINDOW, COLOR_YELLOW, COLOR_BLACK);

    success = true;
done:
    return success;
}

bool display_legend(WINDOW *win) {
    bool success = false;
    if (werase(win) < 0) {
        goto done;
    }
    wrefresh(win);
    refresh();

    wprintw(win, "(o)pen (e)dit (d)elete (r)ename (f)inished (j/k) move");

    wrefresh(win);
    refresh();

    success = true;
done:
    return success;
}

bool display_header(WINDOW *win) {
    bool success = false;
    if (werase(win) < 0) {
        goto done;
    }
    wrefresh(win);
    refresh();

    wprintw(win, "%s | %s | %s", PRIORITY_HEADER, DONE_HEADER, TASK_HEADER);

    wrefresh(win);
    refresh();

    success = true;
done:
    return success;
}

bool redraw_windows(WINDOW *header_win, WINDOW *main_win, WINDOW *legend_win) {
    bool success = true;
    unsigned int dummy = 0; /* Assume it didn't change */
    if(!display_header(header_win)) {
        goto done;
    }
    if(!display_list(main_win, &dummy)) {
        goto done;
    }
    if(!display_legend(legend_win)) {
        goto done;
    }
    success = true;
done:
    return success;
}

bool select_list(WINDOW *header_win, WINDOW *main_win, WINDOW *legend_win) {
    bool success = false;
    int key = 0;
    unsigned int entry = 0;
    unsigned int max_entry = 0;
    int x0 = 0, y0 = 0;

    if(!display_list(main_win, &max_entry)) {
        goto done;
    }
    if (!redraw_windows(header_win, main_win, legend_win)) {
        goto done;
    }
    keypad(main_win, TRUE); /* Enable special characters (arrows, backspace...) */
    wmove(main_win, x0, y0); /* move cursor to beginning of list */
    do {
        highlight_entry(main_win, entry);
        key = wgetch(main_win);    /* Wait for input */
        switch (key) {
            case 107: /* k */
            case KEY_UP:
                entry = (entry == 0)? max_entry-1 : entry-1;
                break;
            case 106: /* j */
            case KEY_DOWN:
                entry = (entry == max_entry-1)? 0 : entry+1;
                break;
            case 74: /* J */
                entry = max_entry - 1;
                break;
            case 75: /* K */
                entry = 0;
                break;
            case 111: /* o */
                display_task(main_win, entry);
                if (!redraw_windows(header_win, main_win, legend_win)) {
                    goto done;
                }
                break;
            case 101: /* e */
                edit_task(main_win, entry);
                toggle_finish(main_win, entry);
                if (!redraw_windows(header_win, main_win, legend_win)) {
                    goto done;
                }
                break;
            case 100: /* d */
                delete_task(main_win, entry);
                max_entry--;
                if (!redraw_windows(header_win, main_win, legend_win)) {
                    goto done;
                }
                break;
            case 97: /* a */
                add_task(main_win, &max_entry);
                if (!redraw_windows(header_win, main_win, legend_win)) {
                    goto done;
                }
                break;
            case 114: /* r */
                rename_task(main_win, entry);
                if (!redraw_windows(header_win, main_win, legend_win)) {
                    goto done;
                }
                break;
            case 102: /* f */
                toggle_finish(main_win, entry);
                if (!redraw_windows(header_win, main_win, legend_win)) {
                    goto done;
                }
                break;
            case 112: /* p */
                set_priority(main_win, entry);
                if (!redraw_windows(header_win, main_win, legend_win)) {
                    goto done;
                }
                break;
            case 113: /* q */
                goto done;
                break;
        }
    } while (1);

    success = true;
done:
    return success;
}

/*
 * Important:
 *   Assumes that there is one task per line.
 *   Entry is the line number associated to the task to highlight.
 */
bool highlight_entry(WINDOW *win, int entry) {
    char buffer[LINE_MAX_LENGTH] = {'\0'};
    chtype chbuffer[LINE_MAX_LENGTH];
    int y0, x0;

    getyx (win, y0, x0);
    mvwchgat(win, y0, x0, -1, A_NORMAL, COLOR_NORMAL, NULL);
    mvwinchnstr(win, y0, x0, chbuffer, LINE_MAX_LENGTH);
    mvwaddchstr(win, y0, x0, chbuffer);

    wattron(win, A_STANDOUT);
    mvwinnstr(win, entry, 0, buffer, LINE_MAX_LENGTH);
    mvwprintw(win, entry, 0, "%s", buffer);
    wmove(win, entry, 0);
    wattroff(win, A_STANDOUT);

    wrefresh(win);
    refresh();

    return true;
}

/*
 * This is done with 0 regards to performance. Improve.
 */
bool display_list(WINDOW *win, unsigned int *max_entry) {
    bool success = false;
    FILE *fp_all = NULL;
    FILE *fp_done = NULL;
    FILE *fp_priority = NULL;
    char buffer_all[LINE_MAX_LENGTH] = {'\0'};
    char buffer_done[LINE_MAX_LENGTH] = {'\0'};
    char buffer_priority[LINE_MAX_LENGTH] = {'\0'};
    const char priority_init[10] =
            {' ', ' ', ' ', ' ', ' ', ' ', ' ' , ' ', ' ', '\0'};
    char priority[10];
    bool done;
    int idx = 0;

    if (werase(win) < 0) {
        goto done;
    }
    wrefresh(win);
    refresh();

    fp_all = popen("nt names", "r");
    if (fp_all == NULL) {
        goto done;
    }

    while (fgets(buffer_all, LINE_MAX_LENGTH, fp_all) != NULL) {
        (*max_entry)++;
        done = false;

        fp_done = popen("nt names done", "r");
        if (fp_done == NULL) {
            goto done;
        }
        while (fgets(buffer_done, LINE_MAX_LENGTH, fp_done) != NULL) {
            if (strcmp(buffer_all, buffer_done) == 0) {
                done = true;
                break;
            }
        }
        if (fp_done != NULL) {
            pclose(fp_done);
            fp_done = NULL;
        }

        fp_priority = popen("nt priority", "r");
        if (fp_priority == NULL) {
            goto done;
        }

        strncpy(priority, priority_init, strlen(priority_init));
        while (fgets(buffer_priority, LINE_MAX_LENGTH, fp_priority) != NULL) {
            for (idx=LINE_MAX_LENGTH; idx>=0; idx--) {
                if (buffer_priority[idx] == 'P') {
                    buffer_priority[idx-1] = '\n'; /* Tasks have the \n appended */
                    buffer_priority[idx] = '\0';
                    if (strcmp(buffer_all, buffer_priority) == 0) {
                        strncpy(priority, buffer_priority+idx+1, 1);
                        break;
                    }
                }
            }
        }
        /* priority will have a \n as well: filter it */
        for (idx=0; idx<strlen(priority); idx++) {
            if (priority[idx] == '\n') {
                priority[idx] = '\0';
            }
        }

        if (done) {
            wprintw(win, "%s%s%s", priority, prefix_done, buffer_all);
        } else {
            wprintw(win, "%s%s%s", priority, prefix_undone, buffer_all);
        }

        if (fp_priority != NULL) {
            pclose(fp_priority);
            fp_priority = NULL;
        }
    }
    wrefresh(win);
    refresh();

    success = true;
done:
    if (fp_all != NULL) {
        pclose(fp_all);
        fp_all = NULL;
    }
    if (fp_done != NULL) {
        pclose(fp_done);
        fp_done = NULL;
    }
    if (fp_priority != NULL) {
        pclose(fp_priority);
        fp_priority = NULL;
    }
    return success;
}

/*
 * Important:
 *   Assumes that there is one task per line.
 *   Entry is the line number associated to the task to display.
 */
bool display_task(WINDOW *win, int entry) {
    bool success = false;
    FILE *fp = NULL;
    char command[] = "nt display ";
    char buffer[LINE_MAX_LENGTH] = {'\0'};

    strcpy(buffer, command);
    wmove(win, entry, OFFSET_TO_NAME);
    if (winstr(win, buffer + strlen(command)) < 0) {
        goto done;
    }

    if (werase(win) < 0) {
        goto done;
    }

    fp = popen(buffer, "r");
    if (fp == NULL) {
        goto done;
    }

    while (fgets(buffer, LINE_MAX_LENGTH, fp) != NULL) {
        wprintw(win, "%s", buffer);
    }

    wgetch(win);

    success = true;
done:
    return success;
}

bool edit_task(WINDOW *win, int entry) {
    bool success = false;
    char command[] = "nt edit ";
    char buffer[LINE_MAX_LENGTH];

    strcpy(buffer, command);
    wmove(win, entry, OFFSET_TO_NAME);
    if (winstr(win, buffer + strlen(command)) < 0) {
        goto done;
    }

    if (werase(win) < 0) {
        goto done;
    }

    if (system(buffer) != 0) {
        goto done;
    }

    success = true;
done:
    return success;
}

bool delete_task(WINDOW *win, int entry) {
    bool success = false;
    FILE *fp = NULL;
    char command[] = "nt rm ";
    char buffer[LINE_MAX_LENGTH] = {'\0'};

    strcpy(buffer, command);
    wmove(win, entry, OFFSET_TO_NAME);
    if (winstr(win, buffer + strlen(command)) < 0) {
        goto done;
    }

    fp = popen(buffer, "r");
    if (fp == NULL) {
        goto done;
        fp = NULL;
    }

    success = true;
done:
    if (fp != NULL) {
        pclose(fp);
        fp = NULL;
    }
    return success;
}

bool add_task(WINDOW *win, unsigned int *max_entry) {
    bool success = false;
    FILE *fp = NULL;
    char command_end[] = " -m ''";
    char buffer[LINE_MAX_LENGTH] = {'\0'};
    unsigned int namex = 0, namey = 0, length = 0;
    int read_chars = 0;
    char ch;

    (*max_entry)++;

    buffer[0] = 'n';
    buffer[1] = 't';
    buffer[2] = ' ';
    mvwprintw(win, MAIN_WIN_LINES-1, 0, "Name: ");
    getyx(win, namey, namex);
    wrefresh(win);
    while((ch = getchar()) != 13) {  /* 13 is the carriage return character */
        wechochar(win, ch | A_BOLD);
        /* Exit if escape is pressed */
        if (ch == 27) {
            success = true;
            (*max_entry)--;
            goto done;
        }
    }

    wmove(win, namey, namex);
    read_chars = winstr(win, buffer+3);
    if (read_chars < 0) {
        goto done;
    }
    length += read_chars;
    strcpy(buffer+length, command_end);

    fp = popen(buffer, "r");
    if (fp == NULL) {
        goto done;
    }

    success = true;
done:
    if (fp != NULL) {
        pclose(fp);
        fp = NULL;
    }
    return success;
}

bool rename_task(WINDOW *win, int entry) {
    bool success = false;
    FILE *fp = NULL;
    char command[] = "nt mv ";
    char buffer[LINE_MAX_LENGTH] = {'\0'};
    unsigned int length = 0;
    int read_chars = 0;
    char ch;

    strcpy(buffer, command);
    length += strlen(command);
    wmove(win, entry, OFFSET_TO_NAME);
    read_chars = winstr(win, buffer + length);
    if (read_chars < 0) {
        goto done;
    }
    length += read_chars;
    buffer[length++] = ' ';

    mvwprintw(win, MAIN_WIN_LINES-1, 0, "Name: ");
    wrefresh(win);
    while((ch = getchar()) != 13) {  /* 13 is the carriage return character */
        /* Exit if escape is pressed */
        if (ch == 27) {
            success = true;
            goto done;
        }
        wechochar(win, ch | A_BOLD);
        buffer[length++] = ch;
    }

    fp = popen(buffer, "r");
    if (fp == NULL) {
        goto done;
    }

    success = true;
done:
    if (fp != NULL) {
        pclose(fp);
        fp = NULL;
    }
    return success;
}

bool toggle_finish(WINDOW *win, int entry) {
    bool success = false;
    FILE *fp = NULL;
    char command[] = "nt done ";
    char buffer[LINE_MAX_LENGTH] = {'\0'};

    strcpy(buffer, command);
    wmove(win, entry, OFFSET_TO_NAME);
    if (winstr(win, buffer + strlen(command)) < 0) {
        goto done;
    }

    fp = popen(buffer, "r");
    if (fp == NULL) {
        goto done;
        fp = NULL;
    }

    success = true;
done:
    if (fp != NULL) {
        pclose(fp);
        fp = NULL;
    }
    return success;
}

bool set_priority(WINDOW *win, unsigned int entry) {
    bool success = false;
    FILE *fp = NULL;
    char command[] = "nt priority ";
    char buffer[LINE_MAX_LENGTH] = {'\0'};
    unsigned int length = 0;
    int read_chars = 0;
    char ch;

    strcpy(buffer, command);
    length += strlen(command);
    wmove(win, entry, OFFSET_TO_NAME);
    read_chars = winstr(win, buffer + length);
    if (read_chars < 0) {
        goto done;
    }
    length += read_chars;
    buffer[length++] = ' ';

    mvwprintw(win, MAIN_WIN_LINES-1, 0, "Priority: ");
    wrefresh(win);
    while((ch = getchar()) != 13) {  /* 13 is the carriage return character */
        /* Exit if escape is pressed */
        if (ch == 27) {
            success = true;
            goto done;
        }
        wechochar(win, ch | A_BOLD);
        buffer[length++] = ch;
    }

    fp = popen(buffer, "r");
    if (fp == NULL) {
        goto done;
        fp = NULL;
    }

    success = true;
done:
    if (fp != NULL) {
        pclose(fp);
        fp = NULL;
    }
    return success;
}

bool end_gui() {
    erase();
    refresh();
    endwin();
    return true;
}

/* Notes
 * =====
 *  -(COLS, LINES) constants are the screen width and height.
 *   A screen can however be made of several windows. The size of the window
 *   can be obtained with getmaxyx().
 */
