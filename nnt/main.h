/*
 * The full screen is of size (LINES, COLS) going from (0, 0) to
 * (LINES-1, COLS-1). The maximum window (fullscreen) can be obtained with:
 *     win = newwin(LINES-1, COLS-1, y0, x0);
 *  Where (y0, x0) is the offeset from the top corner, which is represented by
 *  (0, 0).
 */
#define HEADER_WIN_Y0                            (1)
#define HEADER_WIN_X0                            (1)
#define HEADER_WIN_LINES                         (1)
#define HEADER_WIN_COLS                     (COLS-1)

#define LEGEND_WIN_LINES                         (1)
#define LEGEND_WIN_Y0       (LINES-LEGEND_WIN_LINES)
#define LEGEND_WIN_X0                  (MAIN_WIN_X0)
#define LEGEND_WIN_COLS            (HEADER_WIN_COLS)

#define MAIN_WIN_Y0 (HEADER_WIN_Y0+HEADER_WIN_LINES)
#define MAIN_WIN_X0                  (HEADER_WIN_X0)
#define MAIN_WIN_LINES      (LINES - (HEADER_WIN_Y0\
             + HEADER_WIN_LINES + LEGEND_WIN_LINES))
#define MAIN_WIN_COLS              (HEADER_WIN_COLS)

#define PRIORITY_LENGTH                          (9)
#define DONE_LENGTH                              (9)
#define OFFSET_TO_NAME (PRIORITY_LENGTH+DONE_LENGTH)
const char prefix_done[]   = "| [xx] | ";
const char prefix_undone[] = "| [  ] | ";

const char PRIORITY_HEADER[] = "Priority";
const char DONE_HEADER[] = "Done";
const char TASK_HEADER[] = "Task Name";

#define LINE_MAX_LENGTH                          400
#define COLOR_NORMAL                               1
#define COLOR_SELECTION                            2
#define COLOR_MAIN_WINDOW                          3
#define COLOR_HEADER_WINDOW                        4
#define COLOR_LEGEND_WINDOW                        5
