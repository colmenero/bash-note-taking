#ifndef MAIN_H
#define MAIN_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define MAX_LENGTH_INPUT_ACTION        10
#define MAX_LENGTH_FILENAME            20
#define MAX_LENGTH_FILEPATH           100
#define MAX_LENGTH_FULLPATH             \
    MAX_LENGTH_FILEPATH + MAX_LENGTH_FILENAME

/* Inputs struct
 * Contains the parsed arguments
 */
typedef struct {
    /* Configuration */
    char editor        [MAX_LENGTH_INPUT_ACTION];
    char calfile           [MAX_LENGTH_FILENAME];
    char donefile          [MAX_LENGTH_FILENAME];
    char priorityfile      [MAX_LENGTH_FILENAME];
    char path2repo         [MAX_LENGTH_FILEPATH];
    /* Arguments */
    char action        [MAX_LENGTH_INPUT_ACTION];
    char note              [MAX_LENGTH_FILENAME];
} Inputs;

/*
 * Actions struct
 * Matches action names from a fixed struct member to a variable string
 * The final parameter from the command line will be the string value. That way
 * parameters can be easily changed.
 */
typedef struct {
    char add           [MAX_LENGTH_INPUT_ACTION];
    char edit          [MAX_LENGTH_INPUT_ACTION];
    char mv            [MAX_LENGTH_INPUT_ACTION];

} Actions;

extern const Actions actions;

#endif /* MAIN_H */
