#include "main.h"

const Actions actions = {
    .add = "add",
    .edit = "edit",
    .mv = "mv"
};

bool parse_arguments(Inputs **output, int argc, char **argv);
bool add_note(char *path2repo, char *note);

int main(int argc, char **argv) {
    int exit_status = EXIT_FAILURE;
    Inputs *inputs = NULL;

    if(!parse_arguments(&inputs, argc, argv)) {
        goto done;
    }

    if (strcmp(inputs->action, actions.add) == 0) {
        if(!add_note(inputs->path2repo, inputs->note)) {
            goto done;
        }
    }

    exit_status = EXIT_SUCCESS;
done:
    if (inputs != NULL) {
        free(inputs);
    }
    return exit_status;
}

bool parse_arguments(Inputs **output, int argc, char **argv) {
    bool success = false;
    Inputs *inputs = NULL;
    unsigned int length = 0;

    inputs = malloc(sizeof(Inputs));
    if (inputs == NULL) {
        fprintf(stderr, "Error allocating structure");
        goto done;
    }

    /* Default arguments */
    strcpy(inputs->editor, "nvim");
    strcpy(inputs->calfile, ".calendar");
    strcpy(inputs->donefile, ".done");
    strcpy(inputs->priorityfile, ".priority");
    strcpy(inputs->path2repo, "/home/luis/Tools/noteTaking/.myNotes/");
    strcpy(inputs->action, "add");
    strcpy(inputs->note, "NONE");

    /* If no input is given, set to default */
    if (argc == 1) {
        success = true;
        goto done;
    }

    /* Parse actions */
    if (strlen(argv[1]) > MAX_LENGTH_INPUT_ACTION) {
        goto done;
    }
    strncpy(inputs->action, argv[1], strlen(argv[1])+1);

    /* Parse action-dependent arguments */
    if (strcmp(inputs->action, actions.add)  == 0) {
        /* If only action given: create name following pattern */
        if (argc == 2) {
            // TODO
            strcpy(inputs->note, "NONE");
        }
        /* If besides the action another argument: interpret it as the name */
        else if (argc == 3) {
            length = (strlen(argv[2]) > MAX_LENGTH_FILENAME) ?
                    MAX_LENGTH_FILENAME : strlen(argv[2]);
            strncpy(inputs->note, argv[2], length);
        }
    }

    success = true;
done:
    if (success == false && inputs != NULL){
        free(inputs);
        inputs = NULL;
    }
    *output = inputs;
    return success;
}

bool add_note(char *path2repo, char *note) {
    bool success = false;
    FILE *fd = NULL;
    char fullpath[MAX_LENGTH_FULLPATH] = {'\0'};

    strncpy(fullpath, path2repo, MAX_LENGTH_FILEPATH);
    strncpy(fullpath + strlen(fullpath), note, MAX_LENGTH_FILENAME);
    fd = fopen(fullpath,"w");
    if (fd == NULL) {
        goto done;
    }
    fclose(fd);
    fd = NULL;
    success = true;
done:
    if (success == false && fd != NULL) {
        fclose(fd);
        fd = NULL;
    }
    return success;
}
