Small note-taking application written in bash.

Installation
============

First, configure the $editor variable in *nt_bash* to the tool that best suits
you.
By default, nvim is set.

Then, create a symlink to nt_bash under a folder on your path, preferably ~/bin
(user-wide) or /usr/local/bin (system-wide). E.g:
```
cd ~/bin/
ln -s <path2thisrepo>/nt_bash nt
```

Otherwise, and depending on the system, you may have to add the folder to your
path. E.g:
```
export PATH=$PATH:$HOME/bin
```
    
To make it permanent append line to ~/.bashrc or ~/.profile.
After opening a new terminal or logging in again, nt should be ready to use.
To get completions source the ntComplete.sh file from your .bashrc.

Installation of man page:
- Copy nt.7 to the folder /usr/local/man/man7
- Update the man index: sudo mandb

Ncurses interface
=================
Once the nt bash script can be called from a terminal, there is an Ncurses
interface that eases the user experience. I created it as a first contact with
the ncurses library, so it is (even more) experimental.

It calls the nt bash script from C using popen/system. In that process, input
from the user ends up as part of the command executed. As a consequence, it
poses a security risk. For example, a note's name could be crafted to execute
another shell command in addition to the expected one. Make sure the gui
executable is not writeable, that it runs with lowest permissions possible and
keep the notes to yourself.

More information in the *SEI Cert C Coding Standar rule* "ENV33-C. Do not call
system()"
  https://wiki.sei.cmu.edu/confluence/pages/viewpage.action?pageId=87152177

A nice alternative would be to rewrite the bash code in C to avoid these calls.

To compile the interface, run the following command from the ntt folder:
```
gcc main.c -o ntt -lncurses [-Wall -ggdb]
```

![Simple demo](demo/simple.gif)

Nt C replacement
================
The idea was to replace the bash script with a C program.  Not so sure I will
continue with it. The problem is that the call to the editor still needs
system(), so it seems a bit pointless to replace everything but still have
that.
