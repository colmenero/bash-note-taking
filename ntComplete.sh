#!/bin/bash
_nt_complete()
{
    local folder="$(dirname $BASH_SOURCE)/.myNotes/"
    case $COMP_CWORD in
        1)
            COMPREPLY=($(compgen -W "edit list names display done priority find grep ex cal rm" "${COMP_WORDS[COMP_CWORD]}"))
            ;;
        2)
            COMPREPLY=($(compgen -W "$(ls $folder)" "${COMP_WORDS[COMP_CWORD]}"))
            ;;
    esac
    return 0
}
complete -F _nt_complete nt
